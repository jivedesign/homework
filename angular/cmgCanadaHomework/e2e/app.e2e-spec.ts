import { CmgCanadaHomeworkPage } from './app.po';

describe('cmg-canada-homework App', () => {
  let page: CmgCanadaHomeworkPage;

  beforeEach(() => {
    page = new CmgCanadaHomeworkPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
