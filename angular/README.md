# Angular App Challenge

Using Typescript, build Angular application with two pages: Palindrome Checker and Universal Calculator.

## Palindrome Checker
This page needs to have an input field and a text paragraph. Text paragraph should say "Palindrome" if the input field contains a palindrome string, or "Not a palindrome" otherwise. 

## Universal Calculator
This other page has two input fields that take two integer numbers and three fields that correspond to the result of addition, subtraction, and multiplication of the two given numbers.

## Additional Requirements
Two pages should link to each other.

## Tips and recommendations
 - Use Angular CLI tool to get started with Angular (https://github.com/angular/angular-cli)
 - Keep UI simple and clean
 - Make use of Angular's two-way data binding
 